import os
import sys

sys.path.append(os.path.dirname(__file__) + '/../')
import screen
from resources import locators

class CreditScreen(screen.Screen):

    def __init__(self, activity, platform, version):
        screen.Screen.__init__(self, activity, platform, version)

    def enter_card_number(self, number):
        input_field = self.find_element(locators.card_number_type,
                            locators.card_number_text)
        # input_field.clear()
        self.tap(input_field)
        input_field.send_keys(number)
        self.driver.back()

    def enter_cvc(self, number):
        input_field = self.find_element(locators.cvc_type,
                            locators.cvc_text)
        input_field.send_keys(number)
        self.driver.back()

    def enter_month(self, month):
        field = self.find_elements(locators.month_type, locators.month_text)
        field[0].send_keys(month)
        self.driver.back()

    def enter_year(self, year):
        field = self.find_elements(locators.month_type, locators.month_text)
        field[1].send_keys(year)
        self.driver.back()

    def press_next(self):
        next_button = self.find_element(locators.next_type, locators.next_text)
        self.tap(next_button)
