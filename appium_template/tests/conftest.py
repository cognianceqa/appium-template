import pytest
import os
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)

def read_port_list():
    import json
    with open(PATH("../resources/ports.json")) as f:
        return json.load(f)

def pytest_configure(config):
     #read ports list if we are on the master
     if not hasattr(config, "slaveinput"):
        config.portlist = read_port_list()

def pytest_configure_node(node):
    # the master for each node fills slaveinput dictionary
    # which pytest-xdist will transfer to the subprocess
    node.slaveinput["portadr"] = node.config.portlist.pop()

@pytest.fixture(scope="session")
def port(request):
    slaveinput = getattr(request.config, "slaveinput", None)
    if slaveinput is None: # single-process execution
        portadr = read_port_list()[0]
    else: # running in a subprocess here
        portadr = slaveinput["portadr"]
    return Port(portadr)

class Port:
    def __init__(self, portadr):
        self.portadr = portadr

    def __repr__(self):
        return self.portadr