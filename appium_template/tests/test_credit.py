import os
import sys

sys.path.append(os.path.dirname(__file__) + '/../')
import pytest
from screen_objects.welcome_screen import WelcomeScreen
from screen_objects.credit_screen import CreditScreen
from resources import device_platforms


class TestCredit:
    
    @pytest.mark.parametrize(("platform", "version"), device_platforms.device_platforms)
    @pytest.mark.skipif("False")
    def test_start(self, platform, version):
        screen = WelcomeScreen('WelcomeActivity', platform, version)

        # Start Button on welcome Screen
        screen.push_start_button()

        # Assert that we got on the next screen
        cvc_object = screen.find_element('id',
                                   'keyki.creditcardinfo:id/textView1').text
        assert cvc_object == 'CVC'
        screen.sleep(3)
        screen.quit()

    @pytest.mark.parametrize(("platform", "version"), device_platforms.device_platforms)
    @pytest.mark.skipif("False")
    def test_card(self, platform, version):
        screen = CreditScreen('WelcomeActivity', platform, version)
        screen.push_start_button()

        # Enter all needed credentials
        screen.enter_card_number('4716935998829190')
        screen.enter_year('2015')
        screen.enter_month('Nov')
        screen.enter_cvc('456')

        # Press Next button
        screen.press_next()
        screen.sleep(3)
        screen.quit()

if __name__ == '__main__':
    pytest.main([__file__, "-v"])
