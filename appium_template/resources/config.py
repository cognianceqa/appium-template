APPIUM_HOST = 'http://localhost'
APPIUM_PORT = '4444'
APPIUM_PATH = '/wd/hub'
APPIUM_ENV = '%s:%s%s' % (APPIUM_HOST, APPIUM_PORT, APPIUM_PATH)
PATH_TO_APK = '../resources/CreditCardInfo.apk'

APP_PACKAGE = 'keyki.creditcardinfo'
PLATFORM_NAME = 'Android'
PLATFORM_VERSION = '4.2'
DEVICE_NAME = 'Android Emulator'



