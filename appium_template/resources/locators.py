# START BUTTON
start_btn_type = 'class'
start_btn_text = 'android.widget.Button'

# CREDIT CARD
card_number_type = 'class'
card_number_text = 'android.widget.EditText'

cvc_type = 'id'
cvc_text = 'keyki.creditcardinfo:id/cvc'

next_type = 'id'
next_text = 'keyki.creditcardinfo:id/button1'

month_type = 'id'
month_text = 'android:id/numberpicker_input'