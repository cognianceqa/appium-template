Description
==========
This is a test framework, committed to design easy-to-read tests, that can perform simple tap/drag/scroll/find actions.
Its based on an open source appium framework, that holds selenium-like api. As a benefit it is a cross-platform tool, that can execute tests towards android and iOS devices.
Tests can be run in parallel mode, so that the overall execution time will drastically reduce, in comparison with usual mode.

## Structure of the framework
*resources*
- This folder has all the config files, element locators file, as well as the android/iOs APK/ZIP file.

*screen_objects*
- This tool is page objects oriented, and all the page objects are located in this folder. Each screen object holds all the actions that can be performed on this screen.

*tests*
- This is the place to hold all the tests. Tests code is simplified, and easy-to-read. And all the implementation lies within screen objects.

*utils*
- Additional functions, specific calculations and all the supportive stuff is located in here.

Installation
==========

[![Appium](https://badge.fury.io/js/appium-adb.png)](http://appium.io)

The following framework requires appium being installed and some additional python packages listed in dependencies.txt file.
All the info regarding appium installation can be found [here](http://appium.io)

Python packages can be installed by running the following command:
```
pip install -U dependencies.txt
``` 

## Devices installation
Connect all virtual and real devices to your computer or server and ensure that `adb` sees them with this command:
```
$ANDROID_HOME/platform-tools/adb devices
```

Start Selenium Grid server
```
java -jar selenium-server-standalone-2.42.2.jar -role hub
```
Make sure that Selenium Grid server is up and running by going to this page `http://localhost:4444/grid/console`

Start several appium servers, one server per device. And register these devices with selenium grid server
Each server should have a unique `port` (-p) and `bootstrap port` numbers
```
appium --nodeconfig /full/path/to/register.json -a localhost -p 4723 -bp 4729 -U device_id &
```
Where `device_id` is id of the device listed in `adb devices`.
And `register.json` is a configurational json for appium server:
```
{
  "capabilities":
      [
        {
          "browserName": "Android_Emulator",
          "version":"4.2",
          "maxInstances": 1,
          "platform":"ANDROID"
        }
      ],
  "configuration":
  {
    "cleanUpCycle":2000,
    "timeout":30000,
    "proxy": "org.openqa.grid.selenium.proxy.DefaultRemoteProxy",
    "url":"http://localhost:4723/wd/hub",
    "host": "localhost",
    "port": 4723,
    "maxSession": 1,
    "register": true,
    "registerCycle": 5000,
    "hubPort": 4444,
    "hubHost": "localhost"
  }
}
```
Where `platform` and `version` are the main attributes. Based on them selenium grid will forward test to a certain appium server.
Make sure that all appium servers are registered within grid, by going to this page `http://localhost:4444/grid/console`

In case selenium grid runs on a separate instance, please specify its host in these parametrs `configuration.hubHost`, `configuration.hubPort`



## Configuration of the framework

All the configurational files are located in `./resources` folder
Please put .apk (for android)  or .zip (for .ios) in resources folder and adjust following fields in `config.py` file:
```
PATH_TO_APK = '../resources/CreditCardInfo.apk'
APP_PACKAGE = 'keyki.creditcardinfo'
PLATFORM_NAME = 'Android'
```
Put correct path to .apk or .zip file, and specify correct packages and platform name

## Running the tests

Please specify all the platforms that your test should target at, in `device_platforms.py`
Execute following command in tests folder:
```
py.test --dist=each
```

Thus all tests will be targeted on every platfrom mentioned in `device_platforms.py`

